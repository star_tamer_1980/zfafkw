 package com.zfafakw.zfaf.models.all;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ProductsResponse implements Serializable
{

    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("first_page_url")
    @Expose
    private String firstPageUrl;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("last_page")
    @Expose
    private Integer lastPage;
    @SerializedName("last_page_url")
    @Expose
    private String lastPageUrl;
    @SerializedName("next_page_url")
    @Expose
    private Object nextPageUrl;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("per_page")
    @Expose
    private Integer perPage;
    @SerializedName("prev_page_url")
    @Expose
    private Object prevPageUrl;
    @SerializedName("to")
    @Expose
    private Integer to;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProductsResponse() {
    }

    /**
     *
     * @param path
     * @param lastPageUrl
     * @param total
     * @param firstPageUrl
     * @param nextPageUrl
     * @param perPage
     * @param lastPage
     * @param from
     * @param to
     * @param currentPage
     * @param prevPageUrl
     * @param products
     */
    public ProductsResponse(Integer currentPage, String firstPageUrl, Integer from, Integer lastPage, String lastPageUrl, Object nextPageUrl, String path, Integer perPage, Object prevPageUrl, Integer to, Integer total, List<Product> products) {
        super();
        this.currentPage = currentPage;
        this.firstPageUrl = firstPageUrl;
        this.from = from;
        this.lastPage = lastPage;
        this.lastPageUrl = lastPageUrl;
        this.nextPageUrl = nextPageUrl;
        this.path = path;
        this.perPage = perPage;
        this.prevPageUrl = prevPageUrl;
        this.to = to;
        this.total = total;
        this.products = products;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public String getFirstPageUrl() {
        return firstPageUrl;
    }

    public void setFirstPageUrl(String firstPageUrl) {
        this.firstPageUrl = firstPageUrl;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public void setLastPageUrl(String lastPageUrl) {
        this.lastPageUrl = lastPageUrl;
    }

    public Object getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(Object nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Object getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(Object prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "ProductsResponse{" +
                "currentPage=" + currentPage +
                ", firstPageUrl='" + firstPageUrl + '\'' +
                ", from=" + from +
                ", lastPage=" + lastPage +
                ", lastPageUrl='" + lastPageUrl + '\'' +
                ", nextPageUrl=" + nextPageUrl +
                ", path='" + path + '\'' +
                ", perPage=" + perPage +
                ", prevPageUrl=" + prevPageUrl +
                ", to=" + to +
                ", total=" + total +
                ", products=" + products +
                '}';
    }

    public class Product implements Serializable
    {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("cat_id")
        @Expose
        private String catId;
        @SerializedName("type_cat_id")
        @Expose
        private String typeCatId;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("img_other")
        @Expose
        private Object imgOther;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("active")
        @Expose
        private String active;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("whatsapp")
        @Expose
        private String whatsapp;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("views")
        @Expose
        private String views;

        /**
         * No args constructor for use in serialization
         *
         */
        public Product() {
        }

        /**
         *
         * @param whatsapp
         * @param image
         * @param address
         * @param latitude
         * @param description
         * @param active
         * @param title
         * @param userId
         * @param catId
         * @param createdAt
         * @param phone
         * @param typeCatId
         * @param imgOther
         * @param id
         * @param views
         * @param longitude
         * @param updatedAt
         */
        public Product(Integer id, String userId, String catId, String typeCatId, String image, Object imgOther, String title, String description, String active, String phone, String whatsapp, String address, String latitude, String longitude, String createdAt, String updatedAt, String views) {
            super();
            this.id = id;
            this.userId = userId;
            this.catId = catId;
            this.typeCatId = typeCatId;
            this.image = image;
            this.imgOther = imgOther;
            this.title = title;
            this.description = description;
            this.active = active;
            this.phone = phone;
            this.whatsapp = whatsapp;
            this.address = address;
            this.latitude = latitude;
            this.longitude = longitude;
            this.createdAt = createdAt;
            this.updatedAt = updatedAt;
            this.views = views;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getTypeCatId() {
            return typeCatId;
        }

        public void setTypeCatId(String typeCatId) {
            this.typeCatId = typeCatId;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getImgOther() {
            return imgOther;
        }

        public void setImgOther(Object imgOther) {
            this.imgOther = imgOther;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getActive() {
            return active;
        }

        public void setActive(String active) {
            this.active = active;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getWhatsapp() {
            return whatsapp;
        }

        public void setWhatsapp(String whatsapp) {
            this.whatsapp = whatsapp;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getViews() {
            return views;
        }

        public void setViews(String views) {
            this.views = views;
        }

        @Override
        public String toString() {
            return "Product{" +
                    "id=" + id +
                    ", userId='" + userId + '\'' +
                    ", catId='" + catId + '\'' +
                    ", typeCatId='" + typeCatId + '\'' +
                    ", image='" + image + '\'' +
                    ", imgOther=" + imgOther +
                    ", title='" + title + '\'' +
                    ", description='" + description + '\'' +
                    ", active='" + active + '\'' +
                    ", phone='" + phone + '\'' +
                    ", whatsapp='" + whatsapp + '\'' +
                    ", address='" + address + '\'' +
                    ", latitude='" + latitude + '\'' +
                    ", longitude='" + longitude + '\'' +
                    ", createdAt='" + createdAt + '\'' +
                    ", updatedAt='" + updatedAt + '\'' +
                    ", views='" + views + '\'' +
                    '}';
        }
    }

}
