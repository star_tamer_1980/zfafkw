package com.zfafakw.zfaf.models.all;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultRequest implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    private final static long serialVersionUID = 8795026168827236878L;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResultRequest() {
    }

    /**
     *
     * @param message
     * @param status
     */
    public ResultRequest(Boolean status, String message) {
        super();
        this.status = status;
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
