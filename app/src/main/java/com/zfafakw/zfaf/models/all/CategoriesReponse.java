package com.zfafakw.zfaf.models.all;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoriesReponse implements Serializable
{

    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("first_page_url")
    @Expose
    private String firstPageUrl;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("last_page")
    @Expose
    private Integer lastPage;
    @SerializedName("last_page_url")
    @Expose
    private String lastPageUrl;
    @SerializedName("next_page_url")
    @Expose
    private Object nextPageUrl;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("per_page")
    @Expose
    private Integer perPage;
    @SerializedName("prev_page_url")
    @Expose
    private Object prevPageUrl;
    @SerializedName("to")
    @Expose
    private Integer to;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public CategoriesReponse() {
    }

    /**
     *
     * @param path
     * @param lastPageUrl
     * @param total
     * @param firstPageUrl
     * @param nextPageUrl
     * @param perPage
     * @param lastPage
     * @param from
     * @param to
     * @param categories
     * @param currentPage
     * @param prevPageUrl
     */
    public CategoriesReponse(Integer currentPage, String firstPageUrl, Integer from, Integer lastPage, String lastPageUrl, Object nextPageUrl, String path, Integer perPage, Object prevPageUrl, Integer to, Integer total, List<Category> categories) {
        super();
        this.currentPage = currentPage;
        this.firstPageUrl = firstPageUrl;
        this.from = from;
        this.lastPage = lastPage;
        this.lastPageUrl = lastPageUrl;
        this.nextPageUrl = nextPageUrl;
        this.path = path;
        this.perPage = perPage;
        this.prevPageUrl = prevPageUrl;
        this.to = to;
        this.total = total;
        this.categories = categories;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public String getFirstPageUrl() {
        return firstPageUrl;
    }

    public void setFirstPageUrl(String firstPageUrl) {
        this.firstPageUrl = firstPageUrl;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public void setLastPageUrl(String lastPageUrl) {
        this.lastPageUrl = lastPageUrl;
    }

    public Object getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(Object nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Object getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(Object prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "CategoriesReponse{" +
                "currentPage=" + currentPage +
                ", firstPageUrl='" + firstPageUrl + '\'' +
                ", from=" + from +
                ", lastPage=" + lastPage +
                ", lastPageUrl='" + lastPageUrl + '\'' +
                ", nextPageUrl=" + nextPageUrl +
                ", path='" + path + '\'' +
                ", perPage=" + perPage +
                ", prevPageUrl=" + prevPageUrl +
                ", to=" + to +
                ", total=" + total +
                ", categories=" + categories +
                '}';
    }

    public class Category implements Serializable
    {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("type_category")
        @Expose
        private Integer typeCategory;
        @SerializedName("is_hase_sub_category")
        @Expose
        private Integer isHaseSubCategory;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("active")
        @Expose
        private Integer active;
        @SerializedName("sort")
        @Expose
        private Integer sort;

        /**
         * No args constructor for use in serialization
         *
         */
        public Category() {
        }

        /**
         *
         * @param image
         * @param isHaseSubCategory
         * @param name
         * @param description
         * @param active
         * @param id
         * @param sort
         * @param typeCategory
         */
        public Category(Integer id, String name, String description, Integer typeCategory, Integer isHaseSubCategory, String image, Integer active, Integer sort) {
            super();
            this.id = id;
            this.name = name;
            this.description = description;
            this.typeCategory = typeCategory;
            this.isHaseSubCategory = isHaseSubCategory;
            this.image = image;
            this.active = active;
            this.sort = sort;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {return description;}

        public void setDescription(String description) {this.description = description;}

        public Integer getTypeCategory() {
            return typeCategory;
        }

        public void setTypeCategory(Integer typeCategory) {
            this.typeCategory = typeCategory;
        }

        public Integer getIsHaseSubCategory() {
            return isHaseSubCategory;
        }

        public void setIsHaseSubCategory(Integer isHaseSubCategory) {this.isHaseSubCategory = isHaseSubCategory;}

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

        public Integer getSort() {
            return sort;
        }

        public void setSort(Integer sort) {
            this.sort = sort;
        }

        @Override
        public String toString() {
            return "Category{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", description='" + description + '\'' +
                    ", typeCategory=" + typeCategory +
                    ", isHaseSubCategory=" + isHaseSubCategory +
                    ", image='" + image + '\'' +
                    ", active=" + active +
                    ", sort=" + sort +
                    '}';
        }
    }

}


