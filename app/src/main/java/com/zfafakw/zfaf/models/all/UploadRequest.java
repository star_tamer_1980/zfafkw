package com.zfafakw.zfaf.models.all;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadRequest implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("url")
    @Expose
    private String url;
    private final static long serialVersionUID = 1025787339220418011L;

    /**
     * No args constructor for use in serialization
     *
     */
    public UploadRequest() {
    }

    /**
     *
     * @param message
     * @param url
     * @param status
     */
    public UploadRequest(Boolean status, String message, String url) {
        super();
        this.status = status;
        this.message = message;
        this.url = url;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
