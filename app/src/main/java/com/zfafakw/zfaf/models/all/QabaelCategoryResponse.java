package com.zfafakw.zfaf.models.all;
import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QabaelCategoryResponse implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("sort")
    @Expose
    private String sort;
    @SerializedName("cat_type")
    @Expose
    private String catType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    private final static long serialVersionUID = -9085344739889326797L;

    /**
     * No args constructor for use in serialization
     *
     */
    public QabaelCategoryResponse() {
    }

    /**
     *
     * @param image
     * @param createdAt
     * @param catType
     * @param id
     * @param sort
     * @param title
     * @param updatedAt
     */
    public QabaelCategoryResponse(Integer id, String title, String image, String sort, String catType, String createdAt, String updatedAt) {
        super();
        this.id = id;
        this.title = title;
        this.image = image;
        this.sort = sort;
        this.catType = catType;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
