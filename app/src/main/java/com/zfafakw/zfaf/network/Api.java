package com.zfafakw.zfaf.network;



import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.models.all.PageResponse;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.models.all.QabaelCategoryResponse;
import com.zfafakw.zfaf.models.all.ResultRequest;
import com.zfafakw.zfaf.models.all.UploadRequest;
import com.zfafakw.zfaf.ui.qabael.category.QabaelCategoryAdapter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface Api {

    @GET("page/get/{id}")
    Call<PageResponse> get_page(@Path("id") String id);

    @POST("get_main_categories")
    Call<CategoriesReponse> get_main_categories();

    @POST("get_sub_categories/{cat_id}")
    Call<ArrayList<CategoriesReponse.Category>> get_sub_categories(@Path("cat_id") String cat_id);

    @GET("get_products/{cat_id}")
    Call<ProductsResponse> get_list_products_for_category(@Path("cat_id") String cat_id);

    @GET("product/search/{word}")
    Call<ProductsResponse> search(@Path("word") String word);

    @GET("qabael/all")
    Call<CategoriesReponse> get_qabael_all();

    @GET("qabael/list/{cat_id}/{cat_type_id}")
    Call<ProductsResponse> get_list_products_for_qabael(
            @Path("cat_id") String cat_id,
            @Path("cat_type_id") String cat_type_id
    );

    @GET("get_cards/{cat_id}/{cat_type_id}")
    Call<ProductsResponse> get_list_cards(
            @Path("cat_id") String cat_id,
            @Path("cat_type_id") String cat_type_id
    );

    @GET("product/increase_views/{id}")
    Call<ResultRequest> product_increase_views(@Path("id") int id);

    @POST("upload_file")
    @Multipart
    Call<UploadRequest> upload_file(@Part List<MultipartBody.Part> image);

    @POST("product/product_add")
    @Multipart
    Call<ResultRequest> product_add(
            @Part MultipartBody.Part image,
            @Part List<MultipartBody.Part> images,
            @Part("user_id") RequestBody user_id,
            @Part("cat_id") RequestBody cat_id,
            @Part("cat_type_id") RequestBody cat_type_id,
            @Part("name") RequestBody name,
            @Part("description") RequestBody description,
            @Part("phone") RequestBody phone,
            @Part("whatsapp") RequestBody whatsapp,
            @Part("address") RequestBody address,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude
    );
    @POST("product/product_add")
    @Multipart
    Call<ResultRequest> product_qabael_add(
            @Part MultipartBody.Part image,
            @Part List<MultipartBody.Part> images,
            @Part("user_id") RequestBody user_id,
            @Part("cat_id") RequestBody cat_id,
            @Part("cat_type_id") RequestBody cat_type_id,
            @Part("name") RequestBody name,
            @Part("description") RequestBody description
    );

    @GET("qabael/categories")
    Call<ArrayList<QabaelCategoryResponse>> get_list_qabael_category();

}
