package com.zfafakw.zfaf.ui.products.product_add_1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.models.all.ResultRequest;
import com.zfafakw.zfaf.models.all.UploadRequest;
import com.zfafakw.zfaf.network.RetrofitClient;
import com.zfafakw.zfaf.ui.main_category.MainActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class product_add_1 extends baseActivity {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99 ;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_GALLERY_CAPTURE = 2;
    private int REQUEST_GALLERY = 1; // 1=> main image, 2=> album image
    private static final int SELECT_REQUEST_CODE = 1;

    private String cat_id, imagePath;

    private ImageView imageView;
    private ProgressDialog progressDialog;
    private TextView etTitle, etMobile, etWhatsapp, etAddress;
    private EditText etDesciption;
    private Button btnSave, btnCancel;
    private LinearLayout chooseImage, choolseImageAlbum;
    private Bitmap bitmap;
    private String mediaPath = "";
    private ArrayList<String> imagesUploaded = new ArrayList<String>();
    Uri imageUri;
    private CategoriesReponse.Category cat_object;

    ArrayList<String> albumImages = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add_1);
        toolbar = findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.strFormTitleActivity));
        if(getIntent().getExtras() != null) {
            cat_id = String.valueOf(getIntent().getSerializableExtra("cat_id"));
            Toast.makeText(getApplicationContext(), cat_id, Toast.LENGTH_SHORT).show();
        }
        imageView = findViewById(R.id.imgMain);
        progressDialog = new ProgressDialog(product_add_1.this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.strFormAddProduct));
        etTitle = findViewById(R.id.etTitle);
        etMobile = findViewById(R.id.etMobile);
        etWhatsapp = findViewById(R.id.etWhatsapp);
        etAddress = findViewById(R.id.etAddress);
        etDesciption = findViewById(R.id.etDesciption);
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaPath.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please choose image", Toast.LENGTH_SHORT).show();
                    return;
                }else if (etTitle.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please write your title", Toast.LENGTH_SHORT).show();
                    return;
                }else if (etMobile.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please write your Mobile", Toast.LENGTH_SHORT).show();
                    return;
                }else if (etWhatsapp.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please write your whatsapp number", Toast.LENGTH_SHORT).show();
                    return;
                }else if (etAddress.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please write address", Toast.LENGTH_SHORT).show();
                    return;
                }else if (etDesciption.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please write your description", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    progressDialog.show();
                    SendData();
                }
            }
        });
        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        chooseImage = findViewById(R.id.ln_upload);

        chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                REQUEST_GALLERY = 1;
                showDialog();
            }
        });

        choolseImageAlbum = findViewById(R.id.ln_upload_album);
        choolseImageAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                REQUEST_GALLERY = 2;
                showDialog();
            }
        });

        setGalaeryImages();

    }
    public void setGalaeryImages(){

        // Each row in the list stores country name, currency and flag
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();

        for(int i=0;i<albumImages.size();i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt", "Country : ");
            hm.put("cur","Currency : ");
            hm.put("flag", albumImages.get(i) );
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = { "flag","txt","cur" };

        // Ids of views in listview_layout
        int[] to = { R.id.flag,R.id.txt,R.id.cur};

        // Instantiating an adapter to store each items
        // R.layout.listview_layout defines the layout of each item
        SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);

        // Getting a reference to listview of main.xml layout file
        ListView listView = ( ListView ) findViewById(R.id.listview);

        // Setting the adapter to the listView
        listView.setAdapter(adapter);
    }










    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){

            case REQUEST_IMAGE_CAPTURE:
            {
                if(resultCode == RESULT_OK){

                    try {
                        String imageurl = getRealPathFromURI(imageUri);
                        if(REQUEST_GALLERY == 1) {
                            mediaPath = imageurl;
                            imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                        }else{
                            albumImages.add(imageurl);

                            setGalaeryImages();
                        }
                        Toast.makeText(getApplicationContext(), imageurl.toString(), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    bitmap = (Bitmap) data.getExtras().get("data");
//                    imageView.setImageBitmap(bitmap);
//                    progressDialog.show();
//                    ImageUpload(bitmap);

                }

            }
            break;

            case REQUEST_GALLERY_CAPTURE:
            {
                if(resultCode == RESULT_OK){

                    try {
                        // When an Image is picked
                        if (requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK
                                && null != data) {
                            Uri selectedImage = data.getData();
                            String[] filePathColumn = {MediaStore.Images.Media.DATA};
                            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                            assert cursor != null;
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            if(REQUEST_GALLERY == 1) {
                                mediaPath = cursor.getString(columnIndex);

                                imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                            }else{
                                albumImages.add(cursor.getString(columnIndex));
                                setGalaeryImages();
                            }
                            cursor.close();

                        } else {
                            Toast.makeText(this, "You haven't picked Image",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }

                }
            }
            break;
        }

    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void showDialog(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Write your message here.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(CheckPermission()) {
                            ContentValues values = new ContentValues();
                            values.put(MediaStore.Images.Media.TITLE, "New Picture");
                            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                            imageUri = getContentResolver().insert(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                        }
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "Gallary",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(CheckPermission()) {
                            // Create intent to Open Image applications like Gallery, Google Photos
                            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
                            startActivityForResult(galleryIntent, REQUEST_GALLERY_CAPTURE);
                        }
                        dialog.cancel();
                    }
                });
        builder1.setNeutralButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        builder1.setCancelable(true);
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private void SendData() {
        progressDialog.setMessage(getResources().getString(R.string.strFormAddProduct));
        List<MultipartBody.Part> parts = new ArrayList<>();
        if (albumImages.size() != 0){
            for(int i = 0; i < albumImages.size(); i++) {
                File file = new File(albumImages.get(i));
                Log.d("image url", albumImages.get(i));
                RequestBody requestAlbum= RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("images[]", file.getName(), requestAlbum);
                parts.add(part);
            }

        }


        //pass it like this
        File file = new File(mediaPath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

// MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        RetrofitClient.getInstance().getApi().product_add(
                body,
                parts,
                RequestBody.create(MediaType.parse("multipart/form-data"), "1"),
                RequestBody.create(MediaType.parse("multipart/form-data"), cat_id),
                RequestBody.create(MediaType.parse("multipart/form-data"), "0"),
                RequestBody.create(MediaType.parse("multipart/form-data"),  etTitle.getText().toString()),
                RequestBody.create(MediaType.parse("multipart/form-data"), etDesciption.getText().toString()),
                RequestBody.create(MediaType.parse("multipart/form-data"), etMobile.getText().toString()),
                RequestBody.create(MediaType.parse("multipart/form-data"), etWhatsapp.getText().toString()),
                RequestBody.create(MediaType.parse("multipart/form-data"), etAddress.getText().toString()),
                RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                RequestBody.create(MediaType.parse("multipart/form-data"), "")).enqueue(new Callback<ResultRequest>() {
            @Override
            public void onResponse(Call<ResultRequest> call, Response<ResultRequest> response) {
                progressDialog.dismiss();
                Log.e("error", response.body().getMessage());
                Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_LONG);
                finish();
            }

            @Override
            public void onFailure(Call<ResultRequest> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("error", t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT);
            }
        });

    }


    public boolean CheckPermission() {
        if (ContextCompat.checkSelfPermission(product_add_1.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(product_add_1.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(product_add_1.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(product_add_1.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(product_add_1.this,
                    Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(product_add_1.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(product_add_1.this)
                        .setTitle("Permission")
                        .setMessage("Please accept the permissions")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(product_add_1.this,
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        MY_PERMISSIONS_REQUEST_LOCATION);


                                startActivity(new Intent(product_add_1
                                        .this, product_add_1.class));
                                product_add_1.this.overridePendingTransition(0, 0);
                            }
                        })
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(product_add_1.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }

            return false;
        } else {

            return true;

        }
    }
}