package com.zfafakw.zfaf.ui.account;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.KeyValueShared;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.ui.account.auth.login.login;
import com.zfafakw.zfaf.ui.account.auth.register.register;
import com.zfafakw.zfaf.ui.cards.cards_category;
import com.zfafakw.zfaf.ui.main_category.MainActivity;
import com.zfafakw.zfaf.ui.pages.pageActivity;
import com.zfafakw.zfaf.ui.qabael.list.qabael_list;
import com.zfafakw.zfaf.ui.search.search;

public class my_account extends baseActivity {
    protected BottomNavigationView bottomNavigationView;
    private LinearLayout ln_login;
    private LinearLayout ln_register;
    private LinearLayout ln_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);


        ln_login = findViewById(R.id.ln_login);
        ln_register = findViewById(R.id.ln_register);
        ln_logout = findViewById(R.id.ln_logout);

        if(KeyValueShared.getIsLogin(this).equals("true")){
            ln_login.setVisibility(View.GONE);
            ln_register.setVisibility(View.GONE);
            ln_logout.setVisibility(View.VISIBLE);
        }else {
            ln_login.setVisibility(View.VISIBLE);
            ln_register.setVisibility(View.VISIBLE);
            ln_logout.setVisibility(View.GONE);
        }
        bottomNavigationView =findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.account);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                            case R.id.cards:
                                startActivity(new Intent(getApplicationContext(), cards_category.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
//                            case R.id.search:
//                                startActivity(new Intent(getApplicationContext(), search.class));
//                                overridePendingTransition(0,0);
//                                finish();
//                                return true;
                            case R.id.account:
                                return true;
                            case R.id.qabael:
                                startActivity(new Intent(getApplicationContext(), qabael_list.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                        }
                        return false;
                    }
                });
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if(KeyValueShared.getIsLogin(this).equals("true")){
            ln_login.setVisibility(View.GONE);
            ln_register.setVisibility(View.GONE);
            ln_logout.setVisibility(View.VISIBLE);
        }else {
            ln_login.setVisibility(View.VISIBLE);
            ln_register.setVisibility(View.VISIBLE);
            ln_logout.setVisibility(View.GONE);
        }
    }


    public void funcTerms(View view) {
        Intent i = new Intent(getApplicationContext(), pageActivity.class);
        i.putExtra("id", "1");
        startActivity(i);
    }

    public void funcPrivacy(View view) {
        Intent i = new Intent(getApplicationContext(), pageActivity.class);
        i.putExtra("id", "2");
        startActivity(i);
    }

    public void funcHelp(View view) {
        Intent i = new Intent(getApplicationContext(), pageActivity.class);
        i.putExtra("id", "3");
        startActivity(i);
    }

    public void contact(View view) {
        Intent i = new Intent(getApplicationContext(), pageActivity.class);
        i.putExtra("id", "3");
        startActivity(i);
    }

    public void funcAbout(View view) {
        Intent i = new Intent(getApplicationContext(), pageActivity.class);
        i.putExtra("id", "4");
        startActivity(i);
    }

    public void funcAdvertise(View view) {
        Intent i = new Intent(getApplicationContext(), pageActivity.class);
        i.putExtra("id", "5");
        startActivity(i);
    }

    public void funcLogin(View view) {
        Intent i = new Intent(getApplicationContext(), login.class);
        startActivity(i);
    }

    public void funcRegister(View view) {
        Intent i = new Intent(getApplicationContext(), register.class);
        startActivity(i);
    }

    public void funcLogout(View view) {
        Toast.makeText(getApplicationContext(), "logout button", Toast.LENGTH_SHORT).show();
        KeyValueShared.setName(getApplicationContext(), "");
        KeyValueShared.setId(getApplicationContext(), "");
        KeyValueShared.setIsLogin(getApplicationContext(), "false");
        if(!KeyValueShared.getIsLogin(this).equals("true")){
            ln_login.setVisibility(View.VISIBLE);
            ln_register.setVisibility(View.VISIBLE);
            ln_logout.setVisibility(View.GONE);
        }else {
            ln_login.setVisibility(View.GONE);
            ln_register.setVisibility(View.GONE);
            ln_logout.setVisibility(View.VISIBLE);
        }
    }
}