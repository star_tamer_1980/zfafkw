package com.zfafakw.zfaf.ui.cards.cads_list;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.network.RetrofitClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class card_list extends baseActivity {

    private final String TAG = "product list";
    private String cat_type_id, cat_id;
    private RecyclerView recyclerView;
    private cadsListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_list);

        recyclerView = findViewById(R.id.rvList);
        adapter = new cadsListAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        Toast.makeText(getApplicationContext(), "get class", Toast.LENGTH_SHORT).show();
        if(getIntent().getExtras() != null) {
            Toast.makeText(getApplicationContext(), "get intent", Toast.LENGTH_SHORT).show();
            cat_id = String.valueOf(getIntent().getSerializableExtra("cat_id"));
            cat_type_id = String.valueOf(getIntent().getSerializableExtra("cat_type_id"));
            getProducts();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getProducts();
    }
    private void getProducts() {
        RetrofitClient.getInstance().getApi().get_list_cards(cat_id, cat_type_id).enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                Log.d(TAG, "Response success: " + response.body().toString());
                adapter.setList((ArrayList<ProductsResponse.Product>) response.body().getProducts());
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                Log.d(TAG, "Response failed: " + t.getMessage());
            }
        });

    }
}