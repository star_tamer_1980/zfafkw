package com.zfafakw.zfaf.ui.products.product_details.form_1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.helper.siteURL;
import com.zfafakw.zfaf.helper.whatsApp;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.models.all.ResultRequest;
import com.zfafakw.zfaf.network.RetrofitClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

public class product_1_details extends baseActivity {
    private ViewFlipper v_flipper;
    private ProductsResponse.Product productDetails;
    private TextView txtTitle, tvAddress, tvViews, tvComments;
    private EditText etDescription;
    private LinearLayout lnMobile, lnWhatsApp, lnMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_1_details);

        toolbar = findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);


        v_flipper = findViewById(R.id.v_flipper);
        txtTitle = findViewById(R.id.tvTitle);
        txtTitle.setTypeface(typeFont);
        tvAddress = findViewById(R.id.tvAddress);
        tvAddress.setTypeface(typeFont);
        etDescription = findViewById(R.id.etDescription);
        etDescription.setTypeface(typeFont);
        tvViews = findViewById(R.id.tvViews);
        tvComments = findViewById(R.id.tvComments);


        lnMobile = findViewById(R.id.lnMobile);
        lnWhatsApp = findViewById(R.id.lnWhatsApp);
        lnMap = findViewById(R.id.lnMap);


        if(getIntent().getExtras() != null) {
            productDetails = (ProductsResponse.Product) getIntent().getSerializableExtra("product_object");
//            String images[] = {productDetails.getImage()};
            ArrayList<String> images = new ArrayList<>();
            images.add(productDetails.getImage());
            if(productDetails.getImgOther() != null) {
                String[] imgOther = productDetails.getImgOther().toString().split("\\|");
                for (int i = 0; i < imgOther.length; i++) {
                    images.add(siteURL.BASE_URL_images + imgOther[i]);
                    Log.d("img", siteURL.BASE_URL_images + imgOther[i]);
                }
            }


            for(String image : images){
                flipperImages(image, images.size());
            }
            txtTitle.setText(productDetails.getTitle());
            tvAddress.setText(productDetails.getAddress());
            etDescription.setText(productDetails.getDescription());
            tvViews.setText(String.valueOf(Integer.parseInt(productDetails.getViews()) + 1));
            tvComments.setText("0");
            RetrofitClient.getInstance().getApi().product_increase_views(productDetails.getId()).enqueue(new Callback<ResultRequest>() {
                @Override
                public void onResponse(Call<ResultRequest> call, Response<ResultRequest> response) {

                    if (response.body() != null) {
                        String $message =  response.body().getMessage();
                        Log.d("result", $message);
                    }
                }

                @Override
                public void onFailure(Call<ResultRequest> call, Throwable t) {
                    Log.d("result", t.getMessage());
                }
            });
            lnMobile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri number = Uri.parse("tel:"+productDetails.getPhone());
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                    startActivity(callIntent);

                }
            });
            lnWhatsApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        boolean install = whatsApp.appInstalled("com.whatsapp", getApplicationContext());
                        if(install){
                            Uri number = Uri.parse("http://api.whatsapp.com/send?phone="+productDetails.getPhone()+"&text=تم التواصل عبر تطبيق "+getResources().getString(R.string.app_name));
                            Intent i = new Intent(Intent.ACTION_VIEW, number);
                            startActivity(i);
                        }else{
                            makeText(view.getContext(), getResources().getString(R.string.msgWhatsappNotInstall), LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            lnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri gmmIntentUri = Uri.parse("geo:"+productDetails.getLatitude()+"," + productDetails.getLongitude()+"?q=<"+productDetails.getLatitude()+">,<" + productDetails.getLongitude()+">("+productDetails.getTitle()+")");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    if (mapIntent.resolveActivity(getPackageManager()) != null) {
                        startActivity(mapIntent);
                    }

                }
            });
        }
    }
    public void flipperImages(String image, int count){
        ImageView imageView = new ImageView(this);
        Picasso.get().load(image).into(imageView);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        v_flipper.addView(imageView);
        v_flipper.setFlipInterval(4000);
        if(count > 1) {
            v_flipper.setAutoStart(true);
        }
        v_flipper.setInAnimation(this, android.R.anim.slide_in_left);
        v_flipper.setOutAnimation(this, android.R.anim.slide_out_right);

    }
}