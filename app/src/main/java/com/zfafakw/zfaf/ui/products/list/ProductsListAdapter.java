package com.zfafakw.zfaf.ui.products.list;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.models.all.ProductsResponse.Product;
import com.zfafakw.zfaf.ui.products.product_details.form_1.product_1_details;

import java.util.ArrayList;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ProductsViewHolder> {

    private Intent i;
    private ArrayList<ProductsResponse.Product> List = new ArrayList<>();
    private Context mContext;

    @NonNull
    @Override
    public ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsViewHolder holder, int position) {

        holder.txtTitle.setText(List.get(position).getTitle());
        Picasso.get().load(List.get(position).getImage()).into(holder.imgProduct);
//        Glide.with(mContext.getApplicationContext()).load(List.get(position).getImage()).into(holder.imgProduct);
        holder.lnRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = new Intent(view.getContext(), product_1_details.class);
                i.putExtra("product_object", List.get(position));
                view.getContext().startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public void setList(ArrayList<ProductsResponse.Product> List) {
        this.List = List;
        notifyDataSetChanged();
    }

    public class ProductsViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProduct;
        TextView txtTitle;
        LinearLayout lnRow;
        public ProductsViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            txtTitle  = itemView.findViewById(R.id.txtTitle);
            lnRow = itemView.findViewById(R.id.lnRow);
            Typeface type = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/PNU/ArbFONTS-PNU-Bold.ttf");
        }
    }
}
