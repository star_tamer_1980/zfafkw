package com.zfafakw.zfaf.ui.cards.cads_list;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.models.all.ProductsResponse.Product;
import com.zfafakw.zfaf.ui.cards.card_add.card_add;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class cadsListAdapter extends RecyclerView.Adapter<cadsListAdapter.card> {

    private Intent i;
    private ArrayList<Product> List = new ArrayList<>();


    @NonNull
    @Override
    public card onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new card(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull card holder, int position) {
        Picasso.get().load(List.get(position).getImage()).into(holder.imgProduct);
        holder.lnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(view.getContext(), "تم حفظ الصوره", Toast.LENGTH_SHORT).show();


            }
        });
        holder.lnNeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = new Intent(view.getContext(), card_add.class);
                i.putExtra("imageURL", List.get(position).getImage());
                i.putExtra("cat_id", List.get(position).getCatId());
                i.putExtra("cat_type_id", List.get(position).getTypeCatId());
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public void setList(ArrayList<Product> List) {
        this.List = List;
        notifyDataSetChanged();
    }

    public class card extends RecyclerView.ViewHolder {
        ImageView imgProduct;
        LinearLayout lnDownload, lnNeed;
        public card(@NonNull View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            lnDownload = itemView.findViewById(R.id.ln_download);
            lnNeed = itemView.findViewById(R.id.ln_need);
        }
    }
}
