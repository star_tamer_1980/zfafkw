package com.zfafakw.zfaf.ui.qabael.category;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.models.all.QabaelCategoryResponse;
import com.zfafakw.zfaf.ui.products.product_details.form_1.product_1_details;
import com.zfafakw.zfaf.ui.qabael.products_list.product_list_2;

import java.util.ArrayList;

public class QabaelCategoryAdapter extends RecyclerView.Adapter<QabaelCategoryAdapter.catViewHolder> {

    private String cat_id;
    private Intent i;
    private ArrayList<QabaelCategoryResponse> List = new ArrayList<>();
    private Context mContext;
    public String qabelaName = "قبيلة الرشايده";

    @NonNull
    @Override
    public catViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new catViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.qabael_sub_cat, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull catViewHolder holder, int position) {
        holder.txtTitle.setText(List.get(position).getTitle() + " " + qabelaName);
//        Glide.with(mContext.getApplicationContext()).load(List.get(position).getImage()).into(holder.imgProduct);
//        Picasso.get().load(List.get(position).getImage()).into(holder.imgProduct);
        holder.lnRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = new Intent(view.getContext(), product_list_2.class);
                //cat_type_id, cat_id, cat_type_imageUrl, cat_type_title
                i.putExtra("cat_type_id", List.get(position).getId());
                i.putExtra("cat_id", cat_id);
                i.putExtra("cat_type_title", List.get(position).getTitle());
                i.putExtra("cat_type_imageUrl", List.get(position).getImage());
                i.putExtra("product_object", List.get(position));
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public void setList(ArrayList<QabaelCategoryResponse> List, String cat_id) {
        this.cat_id = cat_id;
        this.List = List;
        notifyDataSetChanged();
    }

    public class catViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProduct;
        TextView txtTitle;
        LinearLayout lnRow;
        public catViewHolder(@NonNull View itemView) {
            super(itemView);
//            imgProduct = itemView.findViewById(R.id.imgProduct);
            txtTitle  = itemView.findViewById(R.id.txtTitle);
            lnRow = itemView.findViewById(R.id.lnRow);
            Typeface type = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/PNU/ArbFONTS-PNU-Bold.ttf");
        }
    }
}
