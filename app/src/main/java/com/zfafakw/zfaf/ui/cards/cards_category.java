package com.zfafakw.zfaf.ui.cards;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.ui.account.my_account;
import com.zfafakw.zfaf.ui.cards.cads_list.card_list;
import com.zfafakw.zfaf.ui.main_category.MainActivity;
import com.zfafakw.zfaf.ui.qabael.list.qabael_list;

public class cards_category extends baseActivity {
    protected BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards_category);
        toolbar = findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);




        bottomNavigationView =findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.cards);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                            case R.id.cards:
                                return true;
//                            case R.id.search:
//                                startActivity(new Intent(getApplicationContext(), search.class));
//                                overridePendingTransition(0,0);
//                                finish();
//                                return true;
                            case R.id.account:
                                startActivity(new Intent(getApplicationContext(), my_account.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                            case R.id.qabael:
                                startActivity(new Intent(getApplicationContext(), qabael_list.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                        }
                        return false;
                    }
                });
    }

    public void monasbat(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "15");
        startActivity(i);
    }

    public void zefaf(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "14");
        startActivity(i);
    }

    public void tahneaa(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "17");
        startActivity(i);
    }

    public void faaliat(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "16");
        startActivity(i);
    }

    public void shokr(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "18");
        startActivity(i);
    }

    public void neqaba(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "19");
        startActivity(i);
    }

    public void entkabat(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "20");
        startActivity(i);
    }

    public void andia(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "21");
        startActivity(i);
    }

    public void magless(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "22");
        startActivity(i);
    }

    public void dawaa(View view) {
        Intent i = new Intent(getApplicationContext(), card_list.class);
        i.putExtra("cat_id", "17");
        i.putExtra("cat_type_id", "23");
        startActivity(i);
    }
}