package com.zfafakw.zfaf.ui.qabael.products_list;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.network.RetrofitClient;
import com.zfafakw.zfaf.ui.products.list.ProductsListAdapter;
import com.zfafakw.zfaf.ui.products.product_add_1.product_add_1;
import com.zfafakw.zfaf.ui.qabael.product_add.product_qabael_add;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class product_list_2 extends baseActivity {

    private final String TAG = "product list";
    private String cat_type_id, cat_id, cat_type_imageUrl, cat_type_title;
    private LinearLayout lnAddNew;
    private EditText etMainCategoryDescription;
    private RecyclerView recyclerView;
    private ProductQabaelAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_2);

        recyclerView = findViewById(R.id.rvList);
        adapter = new ProductQabaelAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        lnAddNew = findViewById(R.id.lnAddNew);
        lnAddNew.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), product_qabael_add.class);
                i.putExtra("cat_id", cat_id);
                i.putExtra("cat_type_id", cat_type_id);
                startActivity(i);
            }
        });

        if(getIntent().getExtras() != null) {
            cat_id = String.valueOf(getIntent().getSerializableExtra("cat_id"));
            cat_type_id = String.valueOf(getIntent().getSerializableExtra("cat_type_id"));
            cat_type_title = String.valueOf(getIntent().getSerializableExtra("cat_type_title"));
            cat_type_imageUrl = String.valueOf(getIntent().getSerializableExtra("cat_type_imageUrl"));
            getProducts();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getProducts();
    }
    private void getProducts() {
        RetrofitClient.getInstance().getApi().get_list_products_for_qabael(cat_id, cat_type_id).enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                Log.d(TAG, "Response success: " + response.body().toString());
                adapter.setList((ArrayList<ProductsResponse.Product>) response.body().getProducts());
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                Log.d(TAG, "Response failed: " + t.getMessage());
            }
        });

    }
}