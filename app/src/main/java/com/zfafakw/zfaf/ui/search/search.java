package com.zfafakw.zfaf.ui.search;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.network.RetrofitClient;
import com.zfafakw.zfaf.ui.account.my_account;
import com.zfafakw.zfaf.ui.cards.cards_category;
import com.zfafakw.zfaf.ui.main_category.CategoryListAdapter;
import com.zfafakw.zfaf.ui.main_category.CategoryViewModel;
import com.zfafakw.zfaf.ui.main_category.MainActivity;
import com.zfafakw.zfaf.ui.products.list.ProductsListAdapter;
import com.zfafakw.zfaf.ui.products.product_add_1.product_add_1;
import com.zfafakw.zfaf.ui.qabael.list.qabael_list;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class search extends baseActivity {
    protected BottomNavigationView bottomNavigationView;
    private final String TAG = "product list";
    private CategoriesReponse.Category cat_object;
    private RecyclerView recyclerView;
    private ProductsListAdapter adapter;
    private Button btn_search;
    private EditText et_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        toolbar = findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        et_search = findViewById(R.id.et_search);
        btn_search = findViewById(R.id.btn_search);

        recyclerView = findViewById(R.id.rvList);
        adapter = new ProductsListAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_search.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.searchIsEmpty), Toast.LENGTH_SHORT).show();
                }else {
                    search(et_search.getText().toString());
                }
            }
        });


        bottomNavigationView =findViewById(R.id.bottom_navigation);
//        bottomNavigationView.setSelectedItemId(R.id.search);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                            case R.id.cards:
                                startActivity(new Intent(getApplicationContext(), cards_category.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
//                            case R.id.search:
//                                return true;
                            case R.id.account:
                                startActivity(new Intent(getApplicationContext(), my_account.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                            case R.id.qabael:
                                startActivity(new Intent(getApplicationContext(), qabael_list.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                        }
                        return false;
                    }
                });

    }
    private void search(String $word) {
        RetrofitClient.getInstance().getApi().search($word).enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                Log.d(TAG, "Response success: " + response.body().toString());
            adapter.setList((ArrayList<ProductsResponse.Product>) response.body().getProducts());
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                Log.d(TAG, "Response failed: " + t.getMessage());
            }
        });
    }

//    RetrofitClient.getInstance().getApi().get_list_products_for_category(String.valueOf(cat_object.getId())).enqueue(new Callback<ProductsResponse>() {
//        @Override
//        public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
//
//            Log.d(TAG, "Response success: " + response.body().toString());
//            adapter.setList((ArrayList<ProductsResponse.Product>) response.body().getProducts());
//        }
//
//        @Override
//        public void onFailure(Call<ProductsResponse> call, Throwable t) {
//            Log.d(TAG, "Response failed: " + t.getMessage());
//        }
//    });

}