package com.zfafakw.zfaf.ui.sub_category;

import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.ui.products.list.products_list;

import java.util.ArrayList;

public class CategorySubListAdapter extends RecyclerView.Adapter<CategorySubListAdapter.CategorySubViewHolder> {

    private Intent i;
//    private ArrayList<CategorySubResponse> CategorySubList = new ArrayList<>();
    private ArrayList<CategoriesReponse.Category> CategorySubList = new ArrayList<>();

    @NonNull
    @Override
    public CategorySubListAdapter.CategorySubViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategorySubViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategorySubListAdapter.CategorySubViewHolder holder, int position) {
        holder.tvCategoryTitle.setText(CategorySubList.get(position).getName());
//        Picasso.get().load(CategorySubList.get(position).getImage()).into(holder.imgCategoryPhoto);
        holder.lnRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i = new Intent(view.getContext(), products_list.class);
                i.putExtra("cat_object", CategorySubList.get(position));
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return CategorySubList.size();
    }
    public void setList(ArrayList<CategoriesReponse.Category> CategorySubList){
        this.CategorySubList = CategorySubList;
        notifyDataSetChanged();
    }

    public class CategorySubViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategoryTitle;
        ImageView imgCategoryPhoto;
        LinearLayout lnRow;
        public CategorySubViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryTitle = itemView.findViewById(R.id.txtTitle);
            imgCategoryPhoto = itemView.findViewById(R.id.imgCategory);
            lnRow = itemView.findViewById(R.id.lnRow);
            Typeface type = Typeface.createFromAsset(itemView.getContext().getAssets(),"fonts/PNU/ArbFONTS-PNU-Bold.ttf");
            tvCategoryTitle.setTypeface(type);
        }
    }
}
