package com.zfafakw.zfaf.ui.cards.card_add;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.models.all.ResultRequest;
import com.zfafakw.zfaf.network.RetrofitClient;
import com.zfafakw.zfaf.ui.products.product_add_1.product_add_1;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class card_add extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99 ;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_GALLERY_CAPTURE = 2;
    private int REQUEST_GALLERY = 1; // 1=> main image, 2=> album image
    private static final int SELECT_REQUEST_CODE = 1;

    private String cat_id = "", cat_type_id = "";
    private ImageView imageView;
    private ProgressDialog progressDialog;
    private TextView etTitle;
    private EditText etDesciption;
    private Button btnSave, btnCancel;
    private LinearLayout chooseImage;
    private Bitmap bitmap;
    private String mediaPath = "";
    Uri imageUri;
    ArrayList<String> albumImages = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_add);
        imageView = findViewById(R.id.imgMain);
        if(getIntent().getExtras() != null) {
            mediaPath = String.valueOf(getIntent().getSerializableExtra("imageURL"));
            cat_id = String.valueOf(getIntent().getSerializableExtra("cat_id"));
            cat_type_id = String.valueOf(getIntent().getSerializableExtra("cat_type_id"));

            Picasso.get().load(mediaPath).into(imageView);

        }
        progressDialog = new ProgressDialog(card_add.this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.strFormAddProduct));
        etTitle = findViewById(R.id.etTitle);
        etDesciption = findViewById(R.id.etDesciption);
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaPath.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please choose image", Toast.LENGTH_SHORT).show();
                    return;
                }else if (etTitle.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please write your title", Toast.LENGTH_SHORT).show();
                    return;
                }else if (etDesciption.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please write your description", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    progressDialog.show();
                    SendData();
                }
            }
        });
        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        chooseImage = findViewById(R.id.ln_upload);

        chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                REQUEST_GALLERY = 1;
                showDialog();
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){

            case REQUEST_IMAGE_CAPTURE:
            {
                if(resultCode == RESULT_OK){

                    try {
                        String imageurl = getRealPathFromURI(imageUri);
                        mediaPath = imageurl;
                        imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    bitmap = (Bitmap) data.getExtras().get("data");
//                    imageView.setImageBitmap(bitmap);
//                    progressDialog.show();
//                    ImageUpload(bitmap);

                }

            }
            break;

            case REQUEST_GALLERY_CAPTURE:
            {
                if(resultCode == RESULT_OK){

                    try {
                        // When an Image is picked
                        if (requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK
                                && null != data) {
                            Uri selectedImage = data.getData();
                            String[] filePathColumn = {MediaStore.Images.Media.DATA};
                            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                            assert cursor != null;
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            mediaPath = cursor.getString(columnIndex);
                            imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));

                            cursor.close();

                        } else {
                            Toast.makeText(this, "You haven't picked Image",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }

                }
            }
            break;
        }

    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void showDialog(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Write your message here.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(CheckPermission()) {
                            ContentValues values = new ContentValues();
                            values.put(MediaStore.Images.Media.TITLE, "New Picture");
                            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                            imageUri = getContentResolver().insert(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                        }
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "Gallary",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(CheckPermission()) {
                            // Create intent to Open Image applications like Gallery, Google Photos
                            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
                            startActivityForResult(galleryIntent, REQUEST_GALLERY_CAPTURE);
                        }
                        dialog.cancel();
                    }
                });
        builder1.setNeutralButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        builder1.setCancelable(true);
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private void SendData() {
        progressDialog.setMessage(getResources().getString(R.string.strFormAddProduct));
        List<MultipartBody.Part> parts = new ArrayList<>();
        //pass it like this
        File file = new File(mediaPath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

// MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        RetrofitClient.getInstance().getApi().product_qabael_add(
                body,
                parts,
                RequestBody.create(MediaType.parse("multipart/form-data"), "1"),
                RequestBody.create(MediaType.parse("multipart/form-data"), cat_id),
                RequestBody.create(MediaType.parse("multipart/form-data"), cat_type_id),
                RequestBody.create(MediaType.parse("multipart/form-data"),  etTitle.getText().toString()),
                RequestBody.create(MediaType.parse("multipart/form-data"), etDesciption.getText().toString())
        ).enqueue(new Callback<ResultRequest>() {
            @Override
            public void onResponse(Call<ResultRequest> call, Response<ResultRequest> response) {
                progressDialog.dismiss();
                Log.e("error", response.body().getMessage().toString());
                Toast.makeText(getApplicationContext(), response.body().toString(), Toast.LENGTH_LONG);
                finish();
            }

            @Override
            public void onFailure(Call<ResultRequest> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage().toString(), Toast.LENGTH_SHORT);
            }
        });


    }


    public boolean CheckPermission() {
        if (ContextCompat.checkSelfPermission(card_add.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(card_add.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(card_add.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(card_add.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(card_add.this,
                    Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(card_add.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(card_add.this)
                        .setTitle("Permission")
                        .setMessage("Please accept the permissions")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(card_add.this,
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        MY_PERMISSIONS_REQUEST_LOCATION);


                                startActivity(new Intent(card_add
                                        .this, product_add_1.class));
                                card_add.this.overridePendingTransition(0, 0);
                            }
                        })
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(card_add.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }

            return false;
        } else {

            return true;

        }
    }

    public void goWhatsapp(View view) {
        String url = "https://api.whatsapp.com/send?phone="+"+212154521";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}