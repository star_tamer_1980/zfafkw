package com.zfafakw.zfaf.ui.products.list;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.network.RetrofitClient;
import com.zfafakw.zfaf.ui.products.product_add_1.product_add_1;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class products_list extends baseActivity {

    private final String TAG = "product list";
    private LinearLayout lnAddNew;
    private CategoriesReponse.Category cat_object;
    private RecyclerView recyclerView;
    private ProductsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);
        toolbar = findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        recyclerView = findViewById(R.id.rvList);
        adapter = new ProductsListAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        lnAddNew = findViewById(R.id.lnAddNew);
        lnAddNew.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), product_add_1.class);
                i.putExtra("cat_id", cat_object.getId());
                startActivity(i);
            }
        });

        if(getIntent().getExtras() != null) {
            cat_object = (CategoriesReponse.Category) getIntent().getSerializableExtra("cat_object");
            getSupportActionBar().setTitle(cat_object.getName());
            getProducts();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProducts();
    }

    private void getProducts() {
        RetrofitClient.getInstance().getApi().get_list_products_for_category(String.valueOf(cat_object.getId())).enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                Log.d(TAG, "Response success: " + response.body().toString());
                adapter.setList((ArrayList<ProductsResponse.Product>) response.body().getProducts());
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                Log.d(TAG, "Response failed: " + t.getMessage());
            }
        });
    }
}