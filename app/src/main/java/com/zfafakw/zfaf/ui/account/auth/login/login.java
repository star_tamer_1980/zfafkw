package com.zfafakw.zfaf.ui.account.auth.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.KeyValueShared;

public class login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void funcRegister(View view) {
        Toast.makeText(getApplicationContext(), "register", Toast.LENGTH_SHORT).show();
    }

    public void forgivPassword(View view) {
        Toast.makeText(getApplicationContext(), "gorgive password", Toast.LENGTH_SHORT).show();
    }

    public void login(View view) {
        Toast.makeText(getApplicationContext(), "Successfully login.", Toast.LENGTH_SHORT).show();
        KeyValueShared.setIsLogin(this, "true");
        KeyValueShared.setId(this, "0");
        KeyValueShared.setName(this, "test");
        finish();
    }
}