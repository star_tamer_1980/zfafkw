package com.zfafakw.zfaf.ui.qabael.category;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.models.all.QabaelCategoryResponse;
import com.zfafakw.zfaf.network.RetrofitClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class list extends baseActivity {


    private CategoriesReponse.Category cat_object;
    private RecyclerView recyclerView;
    private QabaelCategoryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        recyclerView = findViewById(R.id.rvList);
        adapter = new QabaelCategoryAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        if(getIntent().getExtras() != null) {
            cat_object = (CategoriesReponse.Category) getIntent().getSerializableExtra("cat_object");
            getList();
        }

    }

    private void getList() {
        RetrofitClient.getInstance().getApi().get_list_qabael_category().enqueue(new Callback<ArrayList<QabaelCategoryResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<QabaelCategoryResponse>> call, Response<ArrayList<QabaelCategoryResponse>> response) {

                adapter.setList(response.body(), String.valueOf(cat_object.getId()));

            }

            @Override
            public void onFailure(Call<ArrayList<QabaelCategoryResponse>> call, Throwable t) {

            }
        });
    }
}