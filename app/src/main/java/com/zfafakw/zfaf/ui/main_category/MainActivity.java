package com.zfafakw.zfaf.ui.main_category;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.helper.siteURL;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.network.RetrofitClient;
import com.zfafakw.zfaf.ui.account.my_account;
import com.zfafakw.zfaf.ui.cards.cards_category;
import com.zfafakw.zfaf.ui.products.list.products_list;
import com.zfafakw.zfaf.ui.qabael.category.list;
import com.zfafakw.zfaf.ui.qabael.list.qabael_list;
import com.zfafakw.zfaf.ui.search.search;
import com.zfafakw.zfaf.ui.sub_category.category_sub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends baseActivity {

    CategoryViewModel categoryViewModel;
    private final String TAG = "Main Categories";
//    private RecyclerView recyclerView;
    CategoryListAdapter adapter;
    protected BottomNavigationView bottomNavigationView;
    private ViewFlipper v_flipper;
    private ArrayList<CategoriesReponse.Category> CategoryList = new ArrayList<>();
    private Intent i;
    private ImageButton btn_left, btn_right;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        btn_left = findViewById(R.id.btn_left);
//        btn_right = findViewById(R.id.btn_right);
        v_flipper = findViewById(R.id.v_flipper);

        v_flipper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CategoryList.get(v_flipper.getDisplayedChild()).getTypeCategory() == 5) {
                    i = new Intent(view.getContext(), list.class);
                }else{
                    if (CategoryList.get(v_flipper.getDisplayedChild()).getIsHaseSubCategory() == 1) {
                        i = new Intent(view.getContext(), category_sub.class);
                    } else {
                        i = new Intent(view.getContext(), products_list.class);
                    }
                }

                i.putExtra("cat_object", CategoryList.get(v_flipper.getDisplayedChild()));
                view.getContext().startActivity(i);

            }
        });
        v_flipper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    return false;
            }
        });
//        btn_left.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                v_flipper.setInAnimation(MainActivity.this, R.anim.slide_in_right);
//                v_flipper.setOutAnimation(MainActivity.this, R.anim.slide_out_left);
//                v_flipper.showPrevious();
//            }
//        });
//        btn_right.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                v_flipper.setInAnimation(MainActivity.this, android.R.anim.slide_in_left);
//                v_flipper.setOutAnimation(MainActivity.this, android.R.anim.slide_out_right);
//                v_flipper.showNext();
//            }
//        });
        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
//        recyclerView = findViewById(R.id.rvList);
        adapter = new CategoryListAdapter();
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setAdapter(adapter);

        bottomNavigationView =findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                return true;
                            case R.id.cards:
                                startActivity(new Intent(getApplicationContext(), cards_category.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
//                            case R.id.search:
//                                startActivity(new Intent(getApplicationContext(), search.class));
//                                overridePendingTransition(0,0);
//                                finish();
//                                return true;
                            case R.id.account:
                                startActivity(new Intent(getApplicationContext(), my_account.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                            case R.id.qabael:
                                startActivity(new Intent(getApplicationContext(), qabael_list.class));
                                overridePendingTransition(0,0);
                                finish();
                                return true;
                        }
                        return false;
                    }
                });


        getCategoriees();
    }

    public void flipperImages(String image){
        Log.d("image name: ", image);
        ImageView imageView = new ImageView(this);
        Picasso.get().load(image).into(imageView);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        v_flipper.addView(imageView);
        v_flipper.setFlipInterval(9000);
        v_flipper.setAutoStart(true);
        v_flipper.setInAnimation(this, android.R.anim.slide_in_left);
        v_flipper.setOutAnimation(this, android.R.anim.slide_out_right);

    }
    public void getCategoriees(){
        RetrofitClient.getInstance().getApi().get_main_categories().enqueue(new Callback<CategoriesReponse>() {
            @Override
            public void onResponse(Call<CategoriesReponse> call, Response<CategoriesReponse> response) {
                Log.d(TAG, "Response success: " + response.body().toString());
                adapter.setList((ArrayList<CategoriesReponse.Category>) response.body().getCategories());
                CategoryList = adapter.getImageList();


                for(CategoriesReponse.Category cat: CategoryList){
                    flipperImages(cat.getImage());
                }

                v_flipper.startFlipping();
            Log.d("flipper is", String.valueOf(v_flipper.isFlipping()));
            }

            @Override
            public void onFailure(Call<CategoriesReponse> call, Throwable t) {

                Log.d(TAG, "Response Failed: " + t.getMessage());
            }
        });
    }

    public void rightSwipe(View view) {
        v_flipper.setInAnimation(MainActivity.this, android.R.anim.slide_in_left);
        v_flipper.setOutAnimation(MainActivity.this, android.R.anim.slide_out_right);
        v_flipper.showNext();
    }

    public void leftSwipe(View view) {
        v_flipper.setInAnimation(MainActivity.this, R.anim.slide_in_right);
        v_flipper.setOutAnimation(MainActivity.this, R.anim.slide_out_left);
        v_flipper.showPrevious();
    }
}