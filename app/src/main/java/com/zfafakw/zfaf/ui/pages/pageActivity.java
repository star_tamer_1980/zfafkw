package com.zfafakw.zfaf.ui.pages;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.models.all.PageResponse;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.network.RetrofitClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class pageActivity extends AppCompatActivity {

    private String id = "0";
    private TextView content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);

        content = findViewById(R.id.tv_content);

        Toast.makeText(getApplicationContext(), "1", Toast.LENGTH_SHORT).show();
        if(getIntent().getExtras() != null) {
            Toast.makeText(getApplicationContext(), "2", Toast.LENGTH_SHORT).show();
            id = getIntent().getStringExtra("id");
            getPage();
        }

    }
    private void getPage() {
        Toast.makeText(getApplicationContext(), "3", Toast.LENGTH_SHORT).show();
        RetrofitClient.getInstance().getApi().get_page(this.id).enqueue(new Callback<PageResponse>() {
            @Override
            public void onResponse(Call<PageResponse> call, Response<PageResponse> response) {

                Toast.makeText(getApplicationContext(), "4", Toast.LENGTH_SHORT).show();
                Log.d("data", "Response success: " + response.body().toString());
//                adapter.setList((ArrayList<ProductsResponse.Product>) response.body().getProducts());
                content.setText(response.body().getContent());
                content.setMovementMethod(new ScrollingMovementMethod());
            }

            @Override
            public void onFailure(Call<PageResponse> call, Throwable t) {
                Log.d("data", "Response failed: " + t.getMessage());
            }
        });
    }
}