package com.zfafakw.zfaf.ui.qabael.product_details;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.helper.whatsApp;
import com.zfafakw.zfaf.models.all.ProductsResponse;
import com.zfafakw.zfaf.models.all.ResultRequest;
import com.zfafakw.zfaf.network.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

public class details extends baseActivity {

    ViewFlipper v_flipper;
    private ProductsResponse.Product productDetails;
    private TextView tvViews, tvComments;
    private EditText etDescription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        toolbar = findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);


        v_flipper = findViewById(R.id.v_flipper);
        v_flipper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), String.valueOf(v_flipper.getTag()), LENGTH_SHORT).show();
            }
        });
        etDescription = findViewById(R.id.etDescription);
        etDescription.setTypeface(typeFont);
        tvViews = findViewById(R.id.tvViews);
        tvComments = findViewById(R.id.tvComments);
        if(getIntent().getExtras() != null) {
            productDetails = (ProductsResponse.Product) getIntent().getSerializableExtra("product_object");
            String images[] = {productDetails.getImage()};

            int i = 0;
            for(String image : images){
                flipperImages(image, i);
                i++;
            }
            etDescription.setText(productDetails.getDescription());
            getSupportActionBar().setTitle(productDetails.getTitle());
            tvViews.setText(String.valueOf(Integer.parseInt(productDetails.getViews()) + 1));
            tvComments.setText("0");
            RetrofitClient.getInstance().getApi().product_increase_views(productDetails.getId()).enqueue(new Callback<ResultRequest>() {
                @Override
                public void onResponse(Call<ResultRequest> call, Response<ResultRequest> response) {

                    if (response.body() != null) {
                        String $message =  response.body().getMessage();
                        Log.d("result", $message);
                    }
                }

                @Override
                public void onFailure(Call<ResultRequest> call, Throwable t) {
                    Log.d("result", t.getMessage());
                }
            });

        }
    }
    public void flipperImages(String image, Integer i){
        ImageView imageView = new ImageView(this);
        Picasso.get().load(image).into(imageView);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        v_flipper.addView(imageView);
        v_flipper.setFlipInterval(4000);
        v_flipper.setAutoStart(true);
        v_flipper.setTag(i);
        v_flipper.setInAnimation(this, android.R.anim.slide_in_left);
        v_flipper.setOutAnimation(this, android.R.anim.slide_out_right);

    }
}