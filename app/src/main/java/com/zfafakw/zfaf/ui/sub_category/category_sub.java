package com.zfafakw.zfaf.ui.sub_category;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.baseActivity;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.network.RetrofitClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class category_sub extends baseActivity {

    private final String TAG = "Sub Categories";
    private Integer cat_id;
    private CategoriesReponse.Category cat_object;
    private RecyclerView recyclerView;
    CategorySubListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_sub);
        recyclerView = findViewById(R.id.rvList);
        adapter = new CategorySubListAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        if(getIntent().getExtras() != null) {
            cat_object = (CategoriesReponse.Category) getIntent().getSerializableExtra("cat_object");
            cat_id = cat_object.getId();
            getSubCategories();
        }
    }

    private void getSubCategories(){
        RetrofitClient.getInstance().getApi().get_sub_categories(String.valueOf(cat_object.getId())).enqueue(new Callback<ArrayList<CategoriesReponse.Category>>() {
            @Override
            public void onResponse(Call<ArrayList<CategoriesReponse.Category>> call, Response<ArrayList<CategoriesReponse.Category>> response) {
                adapter.setList(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<CategoriesReponse.Category>> call, Throwable t) {

            }
        });
    }
}