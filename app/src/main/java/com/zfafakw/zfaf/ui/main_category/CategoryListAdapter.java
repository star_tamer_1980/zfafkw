package com.zfafakw.zfaf.ui.main_category;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.meg7.widget.BaseImageView;
import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;
import com.zfafakw.zfaf.helper.siteURL;
import com.zfafakw.zfaf.models.all.CategoriesReponse;
import com.zfafakw.zfaf.ui.products.list.products_list;
import com.zfafakw.zfaf.ui.qabael.category.list;
import com.zfafakw.zfaf.ui.sub_category.category_sub;

import java.util.ArrayList;
import java.util.HashMap;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryViewHolder> {

    Context mContext;
    private Intent i;
    private ArrayList<CategoriesReponse.Category> CategoryList = new ArrayList<>();
    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {

        holder.txtTitle.setText(CategoryList.get(position).getName());

//        Glide.with(mContext.getApplicationContext()).load(CategoryList.get(position).getImage()).into(holder.imgCategory);
//        Picasso.get().load(CategoryList.get(position).getImage()).into(holder.imgCategory);
        holder.txtCat.setText(CategoryList.get(position).getDescription());
        holder.lnRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CategoryList.get(position).getTypeCategory() == 5) {
                    i = new Intent(view.getContext(), list.class);
                }else{
                    if (CategoryList.get(position).getIsHaseSubCategory() == 1) {
                        i = new Intent(view.getContext(), category_sub.class);
                    } else {
                        i = new Intent(view.getContext(), products_list.class);
                    }
                }

                i.putExtra("cat_object", CategoryList.get(position));
                view.getContext().startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return CategoryList.size();
    }
    public void setList(ArrayList<CategoriesReponse.Category> CategoryList){
        this.CategoryList = CategoryList;
        notifyDataSetChanged();
    }
    public ArrayList<CategoriesReponse.Category> getImageList(){
        return CategoryList;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        ShapeableImageView imgCategory;
        TextView txtTitle,txtCat;
        LinearLayout lnRow;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            imgCategory = itemView.findViewById(R.id.imgCategory);
            txtTitle  = itemView.findViewById(R.id.txtTitle);
            txtCat = itemView.findViewById(R.id.txtCat);
            lnRow = itemView.findViewById(R.id.lnRow);
            Typeface type = Typeface.createFromAsset(itemView.getContext().getAssets(),"fonts/PNU/ArbFONTS-PNU-Bold.ttf");
            txtTitle.setTypeface(type);
        }
    }
}
