package com.zfafakw.zfaf.helper;

import android.annotation.TargetApi;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.zfafakw.zfaf.R;


public class baseActivity extends AppCompatActivity {
    protected Toolbar toolbar;
    protected Typeface typeFont;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        typeFont = Typeface.createFromAsset(getAssets(),"fonts/PNU/ArbFONTS-PNU-Bold.ttf");

    }

}
