package com.zfafakw.zfaf.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class KeyValueShared {
    private SharedPreferences sharedPreferences;
    private static String getKeyAccountLogin = "accountLogin";
    private static String isLogin = "isLogin";
    private static String name = "name";
    private static String id = "0";


    public KeyValueShared(){

    }
    private static SharedPreferences getAccountData(Context context){
        return context.getSharedPreferences(getKeyAccountLogin, Context.MODE_PRIVATE);
    }
    public static String getIsLogin(Context context){
        return getAccountData(context).getString(isLogin, "0");
    }
    public static void setIsLogin(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(isLogin, input);
        editor.commit();
    }
    public static String getName(Context context){
        return getAccountData(context).getString(name, "0");
    }
    public static void setName(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(name, input);
        editor.commit();
    }
    public static String getId(Context context){
        return getAccountData(context).getString(id, "0");
    }
    public static void setId(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(id, input);
        editor.commit();
    }

}
