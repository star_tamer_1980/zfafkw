package com.zfafakw.zfaf.test;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.squareup.picasso.Picasso;
import com.zfafakw.zfaf.R;


public class slider extends AppCompatActivity {
    ViewFlipper v_flipper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);

        String images[] = {"https://media.filgoal.com/news/verylarge/251802_0.jpg",
                "https://media.filgoal.com/news/verylarge/251802_0.jpg",
                "https://media.filgoal.com/news/verylarge/251802_0.jpg"};
        v_flipper = findViewById(R.id.v_flipper);

        for(String image : images){
            flipperImages(image);
        }
    }
    public void flipperImages(String image){
        ImageView imageView = new ImageView(this);
        Picasso.get().load(image).into(imageView);
        v_flipper.addView(imageView);
        v_flipper.setFlipInterval(4000);
        v_flipper.setAutoStart(true);

        v_flipper.setInAnimation(this, android.R.anim.slide_in_left);
        v_flipper.setOutAnimation(this, android.R.anim.slide_out_right);

    }

}